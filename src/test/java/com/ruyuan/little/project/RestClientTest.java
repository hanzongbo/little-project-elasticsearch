package com.ruyuan.little.project;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruyuan.little.project.elasticsearch.LittleProjectElasticsearchApplication;
import com.ruyuan.little.project.elasticsearch.biz.admin.service.AdminGoodsService;
import com.ruyuan.little.project.elasticsearch.biz.common.dao.ElasticsearchDao;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:基于Rest Client API完成CRUD操作
 **/
@SpringBootTest(classes = LittleProjectElasticsearchApplication.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class RestClientTest {

    /**
     * 日志组件
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RestClientTest.class);

    /**
     * es的restClient的API
     */
    @Autowired
    private RestHighLevelClient restClient;

    /**
     * es的restClient的API
     */
    @Autowired
    private ElasticsearchDao elasticsearchDao;

    /**
     * 店铺的mapping
     */
    @Value(value = "classpath:/mapping/goodsIndexSecondVersion.json")
    private Resource goodsIndexMappingResource;

    /**
     * admin模块商品服务组件
     */
    @Autowired
    private AdminGoodsService adminGoodsService;

    /**
     * es测试用的index
     */
    private final static String TEST_INDEX = "test_index";


    /**
     * 初始化 删除索引和创建索引
     *
     * @throws IOException 异常信息
     */
    @Before
    public void init() throws IOException {
        // 删除索引
        this.deleteIndex(TEST_INDEX);
        // 创建索引
        this.createIndex(TEST_INDEX);
    }

    /**
     * 删除索引
     *
     * @param index 索引名称
     * @throws IOException 异常信息
     */
    private void deleteIndex(String index) throws IOException {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(index);
        try {
            AcknowledgedResponse response = restClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
            LOGGER.info("delete index:{} response:{}", index, response.isAcknowledged());
        } catch (RuntimeException e){
            LOGGER.info("delete index exception", e);
        }
    }

    /**
     * 创建索引
     *
     * @param index 索引名称
     * @throws IOException 异常信息
     */
    private void createIndex(String index) throws IOException {
        CreateIndexRequest createIndexRequest = new CreateIndexRequest(index);
        // 分片和副本，使用默认，分片5 副本1
        createIndexRequest.settings(Settings.builder()
                                            .put("index.number_of_shards", 5)
                                            .put("index.number_of_replicas", 1)
        );
        createIndexRequest.mapping("{\n"
                                           + "            \"properties\":{\n"
                                           + "                \"name\":{\n"
                                           + "                    \"type\":\"text\"\n"
                                           + "                }\n"
                                           + "            }\n"
                                           + "        }", XContentType.JSON);
        CreateIndexResponse createIndexResponse = restClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
        LOGGER.info("create index {} response:{}", index, createIndexResponse.isAcknowledged());
    }

    /**
     * 测试新增数据
     */
    @Test
    public void testInsert() throws IOException {
        Data data = new Data("1", "测试数据01");
        this.insertData(data);
    }

    /**
     * 插入数据
     *
     * @param data 数据
     * @throws IOException 异常信息
     */
    private void insertData(Data data) throws IOException {
        IndexRequest indexRequest = new IndexRequest(TEST_INDEX);
        indexRequest.id(data.getId());
        // 强制刷新数据
        indexRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
        indexRequest.source(JSONObject.toJSONString(data), XContentType.JSON);
        IndexResponse indexResponse = restClient.index(indexRequest, RequestOptions.DEFAULT);
        Assert.assertEquals(indexResponse.status(), RestStatus.CREATED);
    }

    /**
     * 测试查询操作
     */
    @Test
    public void testQuery() throws IOException {
        Data data = new Data("2", "测试数据02");
        // 插入数据
        this.insertData(data);

        // 查询数据
        this.queryById(data);
    }

    /**
     * 根据id查询数据
     *
     * @param expectedData 查询数据
     * @throws IOException 异常信息
     */
    private void queryById(Data expectedData) throws IOException {
        SearchRequest searchRequest = new SearchRequest(TEST_INDEX);
        searchRequest.source(new SearchSourceBuilder()
                                     .query(QueryBuilders.termQuery("id", expectedData.getId()))
        );
        SearchResponse searchResponse = restClient.search(searchRequest, RequestOptions.DEFAULT);
        Assert.assertEquals(searchResponse.status(), RestStatus.OK);

        // 查询条数为1
        SearchHits hits = searchResponse.getHits();
        Assert.assertEquals(1, hits.getTotalHits().value);

        // 判断查询数据和插入数据是否相等
        String dataJson = hits.getHits()[0].getSourceAsString();
        Assert.assertEquals(JSON.toJSONString(expectedData), dataJson);

    }

    /**
     * 测试修改操作
     */
    @Test
    public void testUpdate() throws IOException {
        // 插入数据
        Data data = new Data("3", "测试数据03");
        this.insertData(data);


        // 更新数据
        data.setName("测试数据被更新");
        UpdateRequest updateRequest = new UpdateRequest(TEST_INDEX, data.getId());

        updateRequest.doc(JSONObject.toJSONString(data), XContentType.JSON);
        // 强制刷新数据
        updateRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
        UpdateResponse updateResponse = restClient.update(updateRequest, RequestOptions.DEFAULT);
        Assert.assertEquals(updateResponse.status(), RestStatus.OK);

        // 查询更新结果
        this.queryById(data);

    }

    /**
     * 测试删除操作
     */
    @Test
    public void testDelete() throws IOException {
        Data data = new Data("4", "测试数据04");
        this.insertData(data);

        // 删除数据
        DeleteRequest deleteRequest = new DeleteRequest(TEST_INDEX, data.getId());
        // 强制刷新
        deleteRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
        DeleteResponse deleteResponse = restClient.delete(deleteRequest, RequestOptions.DEFAULT);
        Assert.assertEquals(deleteResponse.status(), RestStatus.OK);

        // 查询数据

        SearchRequest searchRequest = new SearchRequest(TEST_INDEX);
        searchRequest.source(new SearchSourceBuilder()
                                     .query(QueryBuilders.termQuery("id", data.getId()))
        );
        SearchResponse searchResponse = restClient.search(searchRequest, RequestOptions.DEFAULT);
        Assert.assertEquals(searchResponse.status(), RestStatus.OK);

        // 查询条数为1
        SearchHits hits = searchResponse.getHits();
        Assert.assertEquals(0, hits.getTotalHits().value);
    }

    /**
     * 测试自动上架商品
     */
    @Test
    public void testAutoOnlineGoods(){
//        adminGoodsService.autoOnlineGoodsSpuByOnlineTime("2021-07-06 00:00:00");
        adminGoodsService.autoOnlineGoodsSpuByOnlineTimeScroll("2021-07-06 00:00:00");
    }

    @Test
    public void testReindex(){
        try {
            // 创建新的商品索引 goods_index_v2
            Boolean created = elasticsearchDao.createIndex("goods_index_v2", "goods_index", goodsIndexMappingResource);

            if (created){
                // 将goods_index_v1的数据，写入goods_index_v2中
                elasticsearchDao.reindex("goods_index_v1","goods_index_v2");

                // 将索引别名指向的索引从goods_index_v1修改为goods_index_v2
                Boolean changed = elasticsearchDao.changeAliasAfterReindex("goods_index",
                        "goods_index_v1", "goods_index_v2");

                Assert.assertEquals(changed, Boolean.TRUE);
            }else {
                Assert.fail();
            }
        } catch (IOException e) {
            LOGGER.error("reindex fail, oldIndex :{} newIndex :{} alias :{}", "goods_index_v1", "goods_index_v2",
                    "goods_index", e);
        }
    }

    /**
     * 测试实体
     */
    static class Data {

        /**
         * 主键id
         */
        private String id;

        /**
         * 名称
         */
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Data(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

}